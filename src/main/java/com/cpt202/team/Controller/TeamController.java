package com.cpt202.team.Controller;

import com.cpt202.team.Models.Team;
import com.cpt202.team.services.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;



@Controller
@RequestMapping("/team")
public class TeamController {

    @Autowired
    private TeamService teamService;

    @GetMapping("/list")
    public String getList(Model model) {
        model.addAttribute("teamList", teamService.getTeamList());
        return "allTeams";
    }

    @GetMapping("/add")
    public String addTeam(Model model) {
        model.addAttribute("team", new Team());
        return "addTeam";
    }

    @PostMapping("/add")
    public String confirmNewTeam(@ModelAttribute("team") Team team) {
        teamService.newTeam(team);
        return "home";
    }
}
